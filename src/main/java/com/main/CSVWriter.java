package com.main;

import com.datasource.Datasource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class CSVWriter {

    private Datasource ds = new Datasource();

    public File write() throws IOException {
        File result = new File("src\\main\\resources\\result.csv");
        FileWriter fw = new FileWriter(result);
        if(result.exists()){
            result.createNewFile();
        }
        Map<String, Integer> calculated = ds.getRecordsFromDB();
        for(Map.Entry<String, Integer> row : calculated.entrySet()){
            fw.write(row.getKey() + "," + row.getValue() + "\n");
        }
        fw.close();
        return result;
    }
}
