package com.datasource;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class Datasource {

    private String db_url = "jdbc:mysql://localhost:3306/sopra_steria_app";
    private String user = "root";
    private String password = "";

    public static final String db_name = "transactions";
    public static final String QUERY_TRANSACTIONS_FROM_DB = "SELECT * FROM " + db_name;


    public Connection connectToDB(){
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(db_url, user, password);
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        if(connection != null) {
            return connection;
        }
        return null;
    }

    public Map<String, Integer> getRecordsFromDB(){
        Map<String, Integer> loadedRecords = new HashMap<>();
        try(Connection connection = connectToDB();
                PreparedStatement queryStatement = connection.prepareStatement(QUERY_TRANSACTIONS_FROM_DB);
                ResultSet resultSet = queryStatement.executeQuery()){
            Integer finalBought = 0;
            Integer finalSupplied = 0;
            while(resultSet.next()){
               String type = resultSet.getString(1);
               int amount = resultSet.getInt(2);
               switch (type){
                   case "buy":
                       finalBought += amount;
                       break;
                   case "supply":
                       finalSupplied += amount;
                       break;
                   default:
                       break;
               }
            }
            loadedRecords.put("supplied", finalSupplied);
            loadedRecords.put("bought", finalBought);
            loadedRecords.put("result", finalSupplied - finalBought);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loadedRecords;
    }
}
