package com.main;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class CSVWriterTest {

   @Test
    void write() throws IOException {
       File file = new CSVWriter().write();
       Scanner scanner = new Scanner(file);
       List<String> read = new ArrayList<>();
       while (scanner.hasNext()){
           read.add(scanner.nextLine());
       }

       assertEquals("[result,60, supplied,172, bought,112]", read.toString());
    }
}